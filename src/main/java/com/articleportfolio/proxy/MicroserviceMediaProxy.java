package com.articleportfolio.proxy;

import com.articleportfolio.service.dto.ImageDTO;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Headers("Content-Type: application/json")
@FeignClient(name = "MICROSERVICE-MEDIA", url = "http://localhost:9102/MICROSERVICE-MEDIA")
public interface MicroserviceMediaProxy {


    /**
     * OBTENIR TOUTES LES IMAGES PAR L'ID DU PARAGRAPHE
     * @param paragraphId
     * @return
     */
    @GetMapping(value = "/media/paragraph/get-all-images/{paragraphId}")
    List<ImageDTO> getListImageByParagraph(@PathVariable("paragraphId") Integer paragraphId);

}
