package com.articleportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "article")
public class ArticleEntity {

    @Id @GeneratedValue
    private Integer id;

    private String title;

    private Integer position;

    @Column(name = "project_id")
    private Integer projectId;

    @Column(name = "proof_of_concept_id")
    private Integer proofOfConceptId;

}
