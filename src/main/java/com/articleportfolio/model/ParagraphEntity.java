package com.articleportfolio.model;

import lombok.*;
import javax.persistence.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
@Entity @Table(name = "paragraph")
public class ParagraphEntity {

    @Id @GeneratedValue
    private Integer id;

    private String subtitle;

    private String paragraph;

    private Integer position;

    @Column(name = "article_id")
    private Integer articleId;
}
