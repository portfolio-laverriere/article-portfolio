package com.articleportfolio.repository;

import com.articleportfolio.model.ParagraphEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ParagraphRepository extends JpaRepository<ParagraphEntity, Integer> {


    /**
     * OBTENIR TOUTES LES PARAGRAPHES PAR L'ID D'UN ARTICLE
     * @param articleId
     * @return
     */
    List<ParagraphEntity> findAllByArticleId(Integer articleId);


    /**
     * SUPPRIMER UN PARAGRAPHE PAR SON ID
     * @param paragraphId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM paragraph" +
            " WHERE paragraph.id= :paragraphId", nativeQuery = true)
    int deleteParagraphById(@Param("paragraphId") Integer paragraphId);
}
