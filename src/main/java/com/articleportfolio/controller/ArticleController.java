package com.articleportfolio.controller;

import com.articleportfolio.exception.ArticleConflictException;
import com.articleportfolio.exception.ArticleNotFoundException;
import com.articleportfolio.exception.ParagraphNotFoundException;
import com.articleportfolio.exception.ResouceNotFoundException;
import com.articleportfolio.model.ResourceEntity;
import com.articleportfolio.model.TagEntity;
import com.articleportfolio.repository.ArticleRepository;
import com.articleportfolio.repository.ParagraphRepository;
import com.articleportfolio.repository.ResourceRepository;
import com.articleportfolio.repository.TagRepository;
import com.articleportfolio.service.dto.ArticleDTO;
import com.articleportfolio.service.dto.ParagraphDTO;
import com.articleportfolio.service.dto.ResourceDTO;
import com.articleportfolio.service.dto.TagDTO;
import com.articleportfolio.service.mapper.ArticleMapper;
import com.articleportfolio.service.mapper.ParagraphMapper;
import com.articleportfolio.service.mapper.ResourceMapper;
import com.articleportfolio.service.mapper.TagMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Api(description = "API POUE LES OPERATIONS CRUD SUR LES ARTICLES")
@RestController
public class ArticleController {

    private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ParagraphRepository paragraphRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private TagRepository tagRepository;


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ------------------------------------- ADD PARAGRAPHE FOR ARTICLE ------------------------------------- */
    @ApiOperation(value = "ADD PARAGRAPH FOR ARTICLE")
    @PostMapping(value = "/article/paragraph/add-paragraph/{articleId}")
    public List<ParagraphDTO> addParagraphForArticle(@RequestBody List<ParagraphDTO> paragraphDTOList,
                                                     @PathVariable Integer articleId) {

        /**
         * JE VERIFIE SI L'ARTICLE EXISTE
         */
        Boolean articleExists = articleRepository.existsById(articleId);

        if (articleExists) {

            for (ParagraphDTO paragraphDTO : paragraphDTOList) {

                /** J'AJOUTE AU PARAGRAPHE L'ID DE L'ARTICLE AUQUEL CELUI-CI EST LIE */
                paragraphDTO.setArticleId(articleId);

                try {
                    /** J'AJOUTE LE PARAGRAPHE, PUIS JE RECUPERE SON IDENTIFIANT */
                    paragraphDTO.setId(ParagraphMapper.INSTANCE.toDTO(
                            paragraphRepository.save(ParagraphMapper.INSTANCE.toEntity(paragraphDTO))).getId());
                } catch (Exception pEX) {
                    logger.error(String.valueOf(pEX));
                }
            }

        } else {
            throw new ParagraphNotFoundException("Article not found for the ID : " + articleId +
                    " - Impossible to add paragraph to the article");
        }
        return paragraphDTOList;
    }


    /* ------------------------------------- ADD RESSOURCE FOR ARTICLE ------------------------------------- */
    @ApiOperation(value = "ADD RESSOURCE FOR ARTICLE")
    @PostMapping(value = "/article/resource/add-resource/{articleId}")
    public List<ResourceDTO> addRessourceForArticle(@RequestBody List<ResourceDTO> resourceDTOList,
                                                    @PathVariable Integer articleId) {

        for (ResourceDTO resourceDTO : resourceDTOList) {

            /**
             * JE VERIFIE SI LA RESSOURCE EST DEJA PRESENTE DANS LA TABLE
             * @see ResourceRepository#findByUrl(String)
             */
            ResourceEntity resourceEntity = resourceRepository.findByUrl(resourceDTO.getUrl());

            /**
             * SI LA RESSOURCE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE ARTICLE DANS
             * LA TABLE DE JOINTURE "RESOURCE_ARTICLE", SINON J'AJOUTE LA NOUVELLE RESSOURCE ET JE CREE LA JOINTURE
             */
            if (resourceEntity != null) {

                /**
                 * JE VERIFIE SI RESOURCE ET ARTICLE SON DEJA LIES
                 * @see ResourceRepository#resourceArticleExists(Integer, Integer)
                 */
                Boolean resourceArticleExists = resourceRepository.resourceArticleExists(resourceEntity.getId(), articleId);

                if (!resourceArticleExists) {

                    try {

                        /**@see ResourceRepository#joinResourceToTheArticle(Integer, Integer) */
                        resourceRepository.joinResourceToTheArticle(resourceEntity.getId(), articleId);
                        resourceDTO.setId(resourceEntity.getId());

                    } catch (DataIntegrityViolationException pEX) {
                        logger.error(String.valueOf(pEX));
                        throw new ArticleNotFoundException("Article not found for the ID : " + articleId +
                                " - Impossible to link the resource to the article");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ArticleConflictException("The resource is already linked to the article : " + articleId);
                }

            } else {

                /**
                 * JE VERIFIE SI L'ARTICLE EXISTE
                 * @see ResourceRepository#articleExists(Integer)
                 */
                Boolean articleExists = resourceRepository.articleExists(articleId);

                if (articleExists) {

                    /** J'AJOUTE LA RESSOURCE, PUIS JE RECUPERE SON IDENTIFIANT */
                    resourceDTO.setId(ResourceMapper.INSTANCE.toDTO(
                            resourceRepository.save(ResourceMapper.INSTANCE.toEntity(resourceDTO))).getId());

                    /**@see ResourceRepository#joinResourceToTheArticle(Integer, Integer) */
                    resourceRepository.joinResourceToTheArticle(resourceDTO.getId(), articleId);

                } else {
                    throw new ArticleNotFoundException("Article not found for the ID : " + articleId +
                            " - Impossible to add the resource and link it to the article");
                }
            }
        }
        return resourceDTOList;
    }

    /* ------------------------------------- ADD TAG FOR ARTICLE ------------------------------------- */
    @ApiOperation(value = "ADD TAG FOR ARTICLE")
    @PostMapping(value = "/article/tag/add-tag/{articleId}")
    public List<TagDTO> addTagForArticle(@RequestBody List<TagDTO> tagDTOList, @PathVariable Integer articleId) {

        for (TagDTO tagDTO : tagDTOList) {

            /**
             * JE VERIFIE SI LE TAG EST DEJA PRESENTE DANS LA TABLE
             * @see TagRepository#findByTagPath
             */
            TagEntity tagEntity = tagRepository.findByTagPath(tagDTO.getTagPath());

            /**
             * SI LE TAG EST DEJA PRESENT DANS LA TABLE, JE JOINS CELUI-CI A LA TABLE ARTICLE DANS
             * LA TABLE DE JOINTURE "TAG_ARTICLE", SINON J'AJOUTE LA NOUVEAU TAG ET JE CREE LA JOINTURE
             */
            if (tagEntity != null) {

                /**
                 * JE VERIFIE SI TAG ET ARTICLE SON DEJA LIES
                 * @see TagRepository#tagArticleExists(Integer, Integer)
                 */
                Boolean tagArticleExists = tagRepository.tagArticleExists(tagEntity.getId(), articleId);

                if (!tagArticleExists) {

                    try {

                        /** @see TagRepository#joinTagToTheArticle(Integer, Integer) */
                        tagRepository.joinTagToTheArticle(tagEntity.getId(), articleId);
                        tagDTO.setId(tagEntity.getId());
                        
                    } catch (DataIntegrityViolationException pEX) {
                        logger.error(String.valueOf(pEX));
                        throw new ArticleNotFoundException("Article not found for the ID : " + articleId +
                                " - Impossible to link the tag to the article");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }

                } else {
                    throw new ArticleConflictException("The tag is already linked to the article : " + articleId);
                }
    
            } else {

                /**
                 * JE VERIFIE SI L'ARTICLE EXISTE
                 * @see TagRepository#articleExists(Integer)
                 */
                Boolean articleExists = tagRepository.articleExists(articleId);

                if (articleExists) {

                    /** J'AJOUTE LE TAG, PUIS JE RECUPERE SON IDENTIFIANT */
                    tagDTO.setId(TagMapper.INSTANCE.toDTO(
                            tagRepository.save(TagMapper.INSTANCE.toEntity(tagDTO))).getId());

                    /** @see TagRepository#joinTagToTheArticle(Integer, Integer) */
                    tagRepository.joinTagToTheArticle(tagDTO.getId(), articleId);

                } else {
                    throw new ArticleNotFoundException("Article not found for the ID : " + articleId +
                            " - Impossible to add the tag and link it to the article");
                }
            }
        }
        return tagDTOList;
    }


                                    /* ===================================== */
                                    /* ============== UPDATE =============== */
                                    /* ===================================== */

    /* -------------------------------------------- UP ARTICLE -------------------------------------------- */
    @ApiOperation(value = "UP ARTICLE")
    @PutMapping(value = "/article/article/up-article")
    public ArticleDTO upArticle(@RequestBody ArticleDTO articleDTO) {

        /**
         * JE VERIFIE SI L'ARTICLE EXISTE
         */
        Boolean articleExists = articleRepository.existsById(articleDTO.getId());

            if (articleExists) {

                try {
                  articleDTO =  ArticleMapper.INSTANCE.toDTO(articleRepository.save(ArticleMapper.INSTANCE.toEntity(articleDTO)));
                } catch (Exception pEX) {
                    logger.error("{}", String.valueOf(pEX));
                }

            } else {
                logger.error("Update failure - article : '" + articleDTO.getTitle() + "' - Is not found");
                throw new ArticleNotFoundException("Update failure - article : '" + articleDTO.getTitle() + "' - Is not found");
            }
        return articleDTO;
    }

    /* ------------------------------------------ UP PARAGRAPH ------------------------------------------ */
    @ApiOperation(value = "UP PARAGRAPH FOR ARTICLE")
    @PutMapping(value = "/article/paragraph/up-paragraph")
    public ParagraphDTO upParagraph(@RequestBody ParagraphDTO paragraphDTO) {

        /**
         * JE VERIFIE SI LE PARAGRAPHE EXISTE
         */
        Boolean paragraphExists = paragraphRepository.existsById(paragraphDTO.getId());

        if (paragraphExists) {

            try {
                paragraphDTO = ParagraphMapper.INSTANCE.toDTO(
                        paragraphRepository.save(ParagraphMapper.INSTANCE.toEntity(paragraphDTO)));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - paragraph : '" + paragraphDTO.getId() + "' - Is not found");
            throw new ParagraphNotFoundException("Update failure - paragraph : '" + paragraphDTO.getId() + "' - Is not found");
        }
        return paragraphDTO;
    }


                                    /* ======================================= */
                                    /* =============== DELETE  =============== */
                                    /* ======================================= */

    /* ------------------------------------- DEL ARTICLE ------------------------------------- */
    @ApiOperation(value = "DEL ARTICLE BY ID")
    @DeleteMapping(value = "/article/article/del-article/{articleId}")
    public String delArticle(@PathVariable Integer articleId) {

        String massageStatus = "";

        try {

            Integer check = articleRepository.deleteArtcileById(articleId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

    /* ------------------------------------- DEL PARAGRAPH ------------------------------------- */
    @ApiOperation(value = "DEL PARAGRAPH")
    @DeleteMapping(value = "/article/paragraph/del-paragraph/{pragraphId}")
    public String delPragraph(@PathVariable Integer pragraphId) {

        String massageStatus = "";

        try {

            Integer check = paragraphRepository.deleteParagraphById(pragraphId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

    /* ------------------------------------- DEL RESSOURCE BY ARTICLE ------------------------------------- */
    @ApiOperation(value = "DEL RESSOURCE BY ARTICLE")
    @DeleteMapping(value = "/article/resource/del-ressource/{resourceId}/{articleId}")
    public String delRessourceByArticle(@PathVariable Integer resourceId, @PathVariable Integer articleId) {

        String massageStatus = "";

        try {

            /** @see ResourceRepository#deleteResourceForArticle(Integer, Integer) */
            Integer check = resourceRepository.deleteResourceForArticle(resourceId, articleId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

    /* ------------------------------------- DEL TAG BY ARTICLE ------------------------------------- */
    @ApiOperation(value = "DEL TAG BY ARTICLE")
    @DeleteMapping(value = "/article/tag/del-tag/{tagId}/{articleId}")
    public String delTagByArticle(@PathVariable Integer tagId, @PathVariable Integer articleId) {

        String massageStatus = "";

        try {

            /** @see TagRepository#deleteTagForArticle(Integer, Integer)  */
            Integer check = tagRepository.deleteTagForArticle(tagId, articleId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

}
