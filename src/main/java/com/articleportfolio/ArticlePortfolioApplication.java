package com.articleportfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.articleportfolio")
public class ArticlePortfolioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticlePortfolioApplication.class, args);
	}

}
