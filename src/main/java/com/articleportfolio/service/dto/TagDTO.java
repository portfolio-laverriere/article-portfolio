package com.articleportfolio.service.dto;

import lombok.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class TagDTO {

    private Integer id;

    private String name;

    private String tagPath;

}
