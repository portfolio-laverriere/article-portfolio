package com.articleportfolio.service.dto;

import lombok.*;
import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ArticleDTO {

    private Integer id;

    private String title;

    private Integer position;

    private Integer projectId;

    private Integer proofOfConceptId;

    private List<ParagraphDTO> paragraphDTOList;

    private List<TagDTO> tagDTOList;

    private List<ResourceDTO> resourceDTOList;


}
