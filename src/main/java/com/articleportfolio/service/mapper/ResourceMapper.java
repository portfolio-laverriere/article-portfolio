package com.articleportfolio.service.mapper;

import com.articleportfolio.model.ResourceEntity;
import com.articleportfolio.service.dto.ResourceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ResourceMapper {

    ResourceMapper INSTANCE = Mappers.getMapper( ResourceMapper.class );

    ResourceDTO toDTO ( ResourceEntity resourceEntity );

    List<ResourceDTO> toDTOList ( List<ResourceEntity> resourceEntityList );

    ResourceEntity toEntity ( ResourceDTO resourceDTO );

    List<ResourceEntity> toEntityList ( List<ResourceDTO> resourceDTOList );
}
