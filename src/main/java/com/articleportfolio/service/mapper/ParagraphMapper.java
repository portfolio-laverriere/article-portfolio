package com.articleportfolio.service.mapper;

import com.articleportfolio.model.ParagraphEntity;
import com.articleportfolio.service.dto.ParagraphDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ParagraphMapper {

    ParagraphMapper INSTANCE = Mappers.getMapper( ParagraphMapper.class );

    ParagraphDTO toDTO ( ParagraphEntity paragraphEntity );

    List<ParagraphDTO> toDTOList ( List<ParagraphEntity> paragraphEntityList );

    ParagraphEntity toEntity ( ParagraphDTO paragraphDTO );

    List<ParagraphEntity> toEntityList ( List<ParagraphDTO> paragraphDTOS );
}
