package com.articleportfolio.service.mapper;

import com.articleportfolio.model.TagEntity;
import com.articleportfolio.service.dto.TagDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface TagMapper {

    TagMapper INSTANCE = Mappers.getMapper( TagMapper.class );

    TagDTO toDTO ( TagEntity tagEntity );

    List<TagDTO> toDTOList ( List<TagEntity> tagEntityList );

    TagEntity toEntity ( TagDTO tagDTO );

    List<TagEntity> toEntityList ( List<TagDTO> tagDTOList );
}
