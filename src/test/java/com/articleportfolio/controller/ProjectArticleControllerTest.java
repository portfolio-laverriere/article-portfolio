package com.articleportfolio.controller;

import com.articleportfolio.exception.ProjectNotFoundException;
import com.articleportfolio.model.ArticleEntity;
import com.articleportfolio.proxy.MicroserviceMediaProxy;
import com.articleportfolio.repository.ArticleRepository;
import com.articleportfolio.repository.ParagraphRepository;
import com.articleportfolio.repository.ResourceRepository;
import com.articleportfolio.repository.TagRepository;
import com.articleportfolio.service.dto.*;
import com.articleportfolio.service.mapper.ArticleMapper;
import com.articleportfolio.service.mapper.ParagraphMapper;
import com.articleportfolio.service.mapper.ResourceMapper;
import com.articleportfolio.service.mapper.TagMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ProjectArticleControllerTest {

    @Mock
    ArticleRepository articleRepository;

    @Mock
    ParagraphRepository paragraphRepository;

    @Mock
    ResourceRepository resourceRepository;

    @Mock
    TagRepository tagRepository;

    @Mock
    MicroserviceMediaProxy microserviceMediaProxy;

    @InjectMocks
    ProjectArticleController projectArticleController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getListArticleByProject() {

        Integer projectIdMock = 1;
        List<ArticleDTO> articleDTOListMock = new ArrayList<>();
        List<ParagraphDTO> paragraphDTOListMock = new ArrayList<>();
        List<ImageDTO> imageDTOListMock = new ArrayList<>();
        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        List<TagDTO> tagDTOListMock = new ArrayList<>();

        /* --------------------------------------- ARTICLE --------------------------------------- */
        ArticleDTO articleDTOMock2 = ArticleDTO.builder().id(2).title("titre 2").position(2).projectId(1).build();

        /* --------------------------------------- PARAGRAPH/ARTICLE --------------------------------------- */
        ParagraphDTO paragraphDTOMock2 = ParagraphDTO.builder().id(5).subtitle("Sous-titre 2").position(2)
                .paragraph("Paragraph 2").articleId(1).build();

        /* --------------------------------------- IMAGE/PARAGRAPH --------------------------------------- */
        ImageDTO imageDTOMock1 = ImageDTO.builder().id(1).name("Nom image 1").imagePath("Path image 1").build();
        imageDTOListMock.add(imageDTOMock1);
        ImageDTO imageDTOMock2 = ImageDTO.builder().id(7).name("Nom image 7").imagePath("Path image 7").build();
        imageDTOListMock.add(imageDTOMock2);
        paragraphDTOMock2.setImageDTOList(imageDTOListMock);
        paragraphDTOListMock.add(paragraphDTOMock2);

        ParagraphDTO paragraphDTOMock1 = ParagraphDTO.builder().id(4).subtitle("Sous-titre 1").position(1)
                .paragraph("Paragraph 1").articleId(1).build();
        paragraphDTOListMock.add(paragraphDTOMock1);

        articleDTOMock2.setParagraphDTOList(paragraphDTOListMock);

        /* --------------------------------------- RESOURCE/PARAGRAPH --------------------------------------- */
        ResourceDTO resourceDTOMock1 = ResourceDTO.builder().id(8).name("Nom ressource 8").url("Url ressource 8").build();
        resourceDTOListMock.add(resourceDTOMock1);
        ResourceDTO resourceDTOMock2 = ResourceDTO.builder().id(9).name("Nom ressource 9").url("Url ressource 9").build();
        resourceDTOListMock.add(resourceDTOMock2);

        articleDTOMock2.setResourceDTOList(resourceDTOListMock);

        /* --------------------------------------- TAG/PARAGRAPH --------------------------------------- */
        TagDTO tagDTOMock1 = TagDTO.builder().id(12).name("Nom tag 12").tagPath("path tag 12").build();
        tagDTOListMock.add(tagDTOMock1);
        TagDTO tagDTOMock2 = TagDTO.builder().id(13).name("Nom tag 13").tagPath("path tag 13").build();
        tagDTOListMock.add(tagDTOMock2);
        articleDTOMock2.setTagDTOList(tagDTOListMock);

        articleDTOListMock.add(articleDTOMock2);

        ArticleDTO articleDTOMock1 = ArticleDTO.builder().id(1).title("titre 1").position(1).projectId(1).build();
        articleDTOListMock.add(articleDTOMock1);

        when(articleRepository.findAllByProjectId(projectIdMock)).thenReturn(ArticleMapper.INSTANCE.toEntityList(articleDTOListMock));
        when(paragraphRepository.findAllByArticleId(any())).thenReturn(ParagraphMapper.INSTANCE.toEntityList(paragraphDTOListMock));
        when(microserviceMediaProxy.getListImageByParagraph(paragraphDTOMock2.getId())).thenReturn(imageDTOListMock);
        when(resourceRepository.findAllResourceByArticleId(articleDTOMock2.getId()))
                .thenReturn(ResourceMapper.INSTANCE.toEntityList(resourceDTOListMock));
        when(tagRepository.findAllTagByArticleId(articleDTOMock2.getId()))
                .thenReturn(TagMapper.INSTANCE.toEntityList(tagDTOListMock));

        final List<ArticleDTO> articleDTOList = projectArticleController.getListArticleByProject(projectIdMock);

        Assertions.assertEquals(articleDTOList.size(), 2);
        Assertions.assertEquals(articleDTOList.get(0).getId(), 1);
        Assertions.assertEquals(articleDTOList.get(1).getId(), 2);

        Assertions.assertEquals(articleDTOList.get(1).getParagraphDTOList().get(0).getId(), 4);
        Assertions.assertEquals(articleDTOList.get(1).getParagraphDTOList().get(1).getId(), 5);

        Assertions.assertEquals(articleDTOList.get(1).getParagraphDTOList().get(1).getImageDTOList().size(), 2);

        Assertions.assertEquals(articleDTOList.get(1).getTagDTOList().size(), 2);
        Assertions.assertEquals(articleDTOList.get(1).getResourceDTOList().size(), 2);

        when(articleRepository.findAllByProjectId(projectIdMock)).thenReturn(null);
        Assertions.assertThrows(ProjectNotFoundException.class, () -> {
            projectArticleController.getListArticleByProject(projectIdMock);
        });
    }

    @Test
    void addArticleForProject() {

        List<ArticleDTO> articleDTOListMock = new ArrayList<>();
        Integer projectIdMock = 3;

        ArticleDTO articleDTOMock = ArticleDTO.builder().title("Titre article 1").position(1).build();
        articleDTOListMock.add(articleDTOMock);
        ArticleEntity articleEntityMock = ArticleEntity.builder().title("Titre article 1").position(1).projectId(3).build();

        when(articleRepository.projectExists(projectIdMock)).thenReturn(true);
        when(articleRepository.save(any())).thenReturn(articleEntityMock);

        final List<ArticleDTO> articleDTOList = projectArticleController.addArticleForProject(articleDTOListMock, projectIdMock);
        Assertions.assertEquals(articleDTOList.get(0).getId(), articleEntityMock.getId());

        when(articleRepository.projectExists(projectIdMock)).thenReturn(false);
        Assertions.assertThrows(ProjectNotFoundException.class, () -> {
            projectArticleController.addArticleForProject(articleDTOListMock, projectIdMock);
        });
    }
}
