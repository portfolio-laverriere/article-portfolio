package com.articleportfolio.controller;

import com.articleportfolio.exception.ResouceNotFoundException;
import com.articleportfolio.exception.ResourceConflictException;
import com.articleportfolio.repository.ResourceRepository;
import com.articleportfolio.service.dto.ResourceDTO;
import com.articleportfolio.service.mapper.ResourceMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ResourceControllerTest {

    @Mock
    ResourceRepository resourceRepository;

    @InjectMocks
    ResourceController resourceController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void upRessource() {

        ResourceDTO resourceDTOMock = ResourceDTO.builder().id(12).name("Nom de la ressource")
                .url("URL de la ressource").build();

        when(resourceRepository.existsById(resourceDTOMock.getId())).thenReturn(true);
        when(resourceRepository.save(any())).thenReturn(ResourceMapper.INSTANCE.toEntity(resourceDTOMock));
        final ResourceDTO resourceDTO = resourceController.upRessource(resourceDTOMock);
        Assertions.assertEquals(resourceDTO.getId(), resourceDTOMock.getId());

        when(resourceRepository.existsById(resourceDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(ResouceNotFoundException.class, () -> {
            resourceController.upRessource(resourceDTOMock);
        });
    }

    @Test
    void delRessource() {

        Integer resourceIdMock = 1;

        when(resourceRepository.resourceUseInTheTableArticle(any())).thenReturn(null);
        when(resourceRepository.deleteResource(resourceIdMock)).thenReturn(1);
        final String checkStatusSuccess = resourceController.delRessource(resourceIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(resourceRepository.resourceUseInTheTableArticle(any())).thenReturn(null);
        when(resourceRepository.deleteResource(resourceIdMock)).thenReturn(0);
        final String checkStatusFailure = resourceController.delRessource(resourceIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");

        when(resourceRepository.resourceUseInTheTableProject(any())).thenReturn("Titre projet");
        Assertions.assertThrows(ResourceConflictException.class, () -> {
            resourceController.delRessource(resourceIdMock);
        });
    }
}
